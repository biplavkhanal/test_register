package assignment;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Registration {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
		driver.manage().window().maximize();
		//Navigating to the webpage
		driver.get("http://automationpractice.com/index.php");
		driver.findElement(By.className("login")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//Registration page
		driver.findElement(By.id("email_create")).sendKeys("biplav.khanal@gmail.com");
		driver.findElement(By.id("SubmitCreate")).click();
		//Registration 
		//Register Form
		//Personal info
		//driver.findElement(By.id("id_gender1")).click();
		WebElement radio1=driver.findElement(By.id("id_gender1"));
		WebElement radio2=driver.findElement(By.id("id_gender2"));
		radio1.click();
		//radio2.click(); enable to test every possibility
		driver.findElement(By.id("customer_firstname")).sendKeys("Biplav");
		driver.findElement(By.id("customer_lastname")).sendKeys("Khanal");
		driver.findElement(By.id("email"));
		driver.findElement(By.id("passwd")).sendKeys("keyhowl");
		//Date of Birth
		WebElement option1=driver.findElement(By.id("days"));
		option1.sendKeys("1");
		WebElement option2=driver.findElement(By.id("months"));
		option2.sendKeys("January");
		WebElement option3=driver.findElement(By.id("years"));
		option3.sendKeys("1997");
		//checkbox
		driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[7]/div/span/input")).click();
		driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[8]/div/span/input")).click();
		System.out.println("Personal information added");
		
		//Address
		driver.findElement(By.name("firstname")).sendKeys("Biplav");
		driver.findElement(By.name("lastname")).sendKeys("Khanal");
		driver.findElement(By.name("company")).sendKeys("TestCo");
		driver.findElement(By.id("address1")).sendKeys("Kapan");
		driver.findElement(By.id("address2")).sendKeys("Testaddress");
		driver.findElement(By.id("city")).sendKeys("Kathmandu");
		 WebElement state1=driver.findElement(By.id("id_state"));
		 state1.sendKeys("alaska");
		 driver.findElement(By.id("postcode")).sendKeys("1233");
		 WebElement country= driver.findElement(By.id("id_country"));
		 country.sendKeys("America");
		 
		 driver.findElement(By.id("other")).sendKeys("Other information space: if you really wanted to add new things");
		 driver.findElement(By.id("phone")).sendKeys("9863721869");
		 driver.findElement(By.id("phone_mobile")).sendKeys("9863721869");
		 driver.findElement(By.id("alias")).sendKeys("Nothing to add more");
		 driver.findElement(By.id("submitAccount")).click();
		 System.out.println("User Resistered successfully" );
		 System.out.println("Completed");
		 driver.close();
		

	}

}
